// Update tasks count

function updateTaskCount() {
	// Incomplete Tasks
	var incompletetaskCount = document.querySelectorAll('#taskList li.incompleteTask').length;
	document.getElementById('tasksLeft').innerHTML = incompletetaskCount;

	// Completed Tasks
	var completedtaskCount = document.querySelectorAll('#taskList li.completedTask').length;
	var removeTasksButton = document.getElementById('removeCompletedTasksButton');
	removeTasksButton.innerHTML = '<span>Remove Completed Tasks: </span><strong>'
								  +completedtaskCount+
								  '</strong>'
	if (completedtaskCount != 0) {
		removeTasksButton.classList.remove('hidden');
	} else if (completedtaskCount == 0) {
		removeTasksButton.classList.add('hidden');
	}
}

// Adding a task to the list

function addTask() {
	var taskInput = document.getElementById('taskInputField').value;
	// validation to check if there is some actual input
	if (/\S/.test(taskInput)) { 
		var taskValue = document.createTextNode(taskInput);
		var li = document.createElement('li');
		li.className = "incompleteTask"
		li.innerHTML = '<form><input type="checkbox" onclick="toggleTaskStatus(this);"><span ondblclick="editTask(this);">'
						+taskInput
						+'</span></form><button>X</button></form>';
		document.getElementById('taskList').appendChild(li);
		updateTaskCount();
		storeList();
		return false;
	} else {
		alert("You need to enter a todo to get crackin!");
		return false;
	}
}

// Edit a task

function updateTask(e) {
	var updatedTaskValue = e.value;
	// validation to check if there is some actual input. Otherwise the task value will be restored
	if (/\S/.test(updatedTaskValue)) {
		e.parentNode.innerHTML = updatedTaskValue;
	} else {
		var currentTaskValue = e.getAttribute('data-current-task-value');
		e.parentNode.innerHTML = currentTaskValue;
	}
}

function editTask(e) {
	var holdTaskValue = (e.innerHTML);
	e.innerHTML = '<input type="text" data-current-task-value="'
				  +holdTaskValue
				  +'" onblur="updateTask(this);">';
}

// Toggle the state of the task

function toggleTaskStatus(e) {
	var taskStatus = e.checked;
	if (taskStatus == true) {
		e.parentNode.parentNode.className = "completedTask";
	} else if (taskStatus == false) {
		e.parentNode.parentNode.className = "incompleteTask";
	}
	updateTaskCount();
	storeList();
}

// Toggle all tasks 

function toggleAllTasks(e) {
	var allTasksList = document.querySelectorAll('#taskList li form input');
	if (e.checked) {
		allTasksList.forEach(check);
		function check (e) {
			if(e.checked) {
				return;
			} else {
				e.click();
			}
		}
	} else {
		allTasksList.forEach(uncheck);
		function uncheck (e) {
			if(e.checked) {
				e.click();
			} else {
				return;
			}
		}
	}
}

// Remove a task from the list

var x = document.getElementById('taskList'); 
x.addEventListener('click', function(e) {
	console.log(e.target.tagName);
	if (e.target.tagName === 'BUTTON') {
		console.log(e.target.parentNode.parentNode);
	} else {
		return;
	}
});


function deleteTask() {
	// e.parentNode.parentNode.removeChild(e.parentNode);
	// updateTaskCount();
	// storeList();
}

// Remove all completed tasks from the list

function deleteAllCompletedTasks() {
	var completedTasksList = document.querySelectorAll('#taskList li.completedTask');
	completedTasksList.forEach(deleteCompletedTasks);
	function deleteCompletedTasks(e) {
		e.parentNode.removeChild(e);
	}
	updateTaskCount()
};

// Store List in localstorage

function storeList() {
	localStorage.setItem('list', document.getElementById('taskList').innerHTML);
}

// Retrieve list from localstorage

function restoreList() {
	if (localStorage.getItem('list')) {
		document.getElementById('taskList').innerHTML = localStorage.getItem('list');
		var allTasksList = document.querySelectorAll('#taskList li.completedTask form input');
		allTasksList.forEach(simulateClick);
		updateTaskCount();
	} else {
		return;
	}
	
}

// Filter Tasks

function removeHiddenProperty (e) {
	e.classList.remove("hidden")
}

function showAllTasks() {
	var allTasksList = document.querySelectorAll('#taskList li');
	allTasksList.forEach(removeHiddenProperty);
}

function hideCompletedTasks() {

	var completedTasksList = document.querySelectorAll('#taskList li.completedTask');
	completedTasksList.forEach(filter);
	function filter(e) {
		e.classList.add("hidden");
	}

	var incompleteTasksList = document.querySelectorAll('#taskList li.incompleteTask');
	incompleteTasksList.forEach(test);
	function test (e) {
		e.classList.remove("hidden");
	}
}

function hideIncompleteTasks() {
	
	var incompleteTasksList = document.querySelectorAll('#taskList li.incompleteTask');
	incompleteTasksList.forEach(test);
	function test (e) {
		e.classList.add("hidden");
	}

	var completedTasksList = document.querySelectorAll('#taskList li.completedTask');
	completedTasksList.forEach(filter);
	function filter (e) {
		e.classList.remove("hidden");
	}
}

// Function to simulate a click

function simulateClick(e) {
	e.click();
}

// Functions to run on page load

window.onload = updateTaskCount; // Count tasks when the page loads
window.onload = restoreList; // Restores the list from local storage